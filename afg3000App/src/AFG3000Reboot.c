/* AFG3000Reboot.c */

/* REBOOT code for Tektronix AFG3000 series
 * Author: Niklas Claesson.
 *****************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>

#include <osiSock.h>
#include <epicsThread.h>

#include <iocsh.h>
#include <epicsExport.h>

int AFG3000Reboot(char * inetAddr)
{
    struct sockaddr_in serverAddr;
    SOCKET fd;
    int status;
    int nbytes;

    errno = 0;
    fd = epicsSocketCreate(PF_INET, SOCK_STREAM, 0);
    if(fd == -1) {
        printf("can't create socket %s\n",strerror(errno));
        return(-1);
    }
    memset((char*)&serverAddr, 0, sizeof(struct sockaddr_in));
    serverAddr.sin_family = PF_INET;
    /* 23 is telnet port */
    status = aToIPAddr(inetAddr,23,&serverAddr);
    if(status) {
        printf("aToIPAddr failed\n");
        return(-1); 
    }
    errno = 0;
    status = connect(fd,(struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if(status) {
        printf("can't connect %s\n",strerror (errno));
        epicsSocketDestroy(fd);
        return(-1);
    }
    /* Credentials to log in */
    nbytes = send(fd,"root\n",5,0);
    if(nbytes!=5) printf("nbytes %d expected 5\n",nbytes);
    epicsThreadSleep(1.0);

    /* Reboot command */
    nbytes = send(fd,"reboot\n",7,0);
    if(nbytes!=7) printf("nbytes %d expected 7\n",nbytes);
    epicsThreadSleep(1.0);
    epicsSocketDestroy(fd);
    epicsThreadSleep(20.0);
    return(0);
}

/* Information needed by iocsh */
static const iocshArg AFG3000RebootArg0 = { "inetAddr",iocshArgString};
static const iocshArg *AFG3000RebootArgs[1] = {&AFG3000RebootArg0};
static const iocshFuncDef AFG3000RebootFuncDef = {"AFG3000Reboot",1,AFG3000RebootArgs};

/* Wrapper called by iocsh, selects the argument types that AFG3000Reboot needs */
static void AFG3000RebootCallFunc(const iocshArgBuf *args)
{
    AFG3000Reboot(args[0].sval);
}

/* Register routine */
static void AFG3000RebootRegister(void) {
    iocshRegister(&AFG3000RebootFuncDef,AFG3000RebootCallFunc);
}

epicsExportRegistrar(AFG3000RebootRegister);
