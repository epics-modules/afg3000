#!../../bin/linux-x86_64/afg3000

#- You may have to change afg3000 to something else
#- everywhere it appears in this file

< envPaths

## Register all support components
dbLoadDatabase("../../dbd/afg3000.dbd",0,0)
afg3000_registerRecordDeviceDriver(pdbbase) 

# streamDevice protocol file location
epicsEnvSet("STREAM_PROTOCOL_PATH","$(TOP)/db")

#DB include for database concatenation
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(TOP)/db")

# Device configurations
epicsEnvSet("AFG_IP",    "$(AFG3000_IP=192.168.1.1)")   # Choose afg ethernet address
epicsEnvSet("AFG_PREFIX", "$(AFG3000_PREFIX=AFG3102C)") # Choose site prefix name
epicsEnvSet("AFG_ASYN_PORT",  "AFG3102C")  # Choose asyn port name
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 100000)

# Setup IOC->hardware link
vxi11Configure("$(AFG_ASYN_PORT)", "$(AFG_IP)", 0, 0.0, "inst0", 0)

# Load records
dbLoadRecords("$(TOP)/db/AFG3000Device.template", "P=$(AFG_PREFIX):,PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(TOP)/db/AFG3000Channel.template", "P=$(AFG_PREFIX):, R=AO0:, CHANNEL=1, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(TOP)/db/AFG3000Channel.template", "P=$(AFG_PREFIX):, R=AO1:, CHANNEL=2, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(TOP)/db/AFG3000Channel-internal.template", "P=$(AFG_PREFIX):, R=CH0:, CHANNEL=3, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(TOP)/db/AFG3000Channel-internal.template", "P=$(AFG_PREFIX):, R=CH1:, CHANNEL=4, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")

# Enable asyn trace
#asynSetTraceMask($(AFG_ASYN_PORT),0, 0x9)
#asynSetTraceIOMask($(AFG_ASYN_PORT),0, 0x2)

# Start IOC
iocInit()
